# symfony/[flex](https://phppackages.org/p/symfony/flex)

[**symfony/flex**](https://packagist.org/packages/symfony/flex)
[![rank](https://pages-proxy.gitlab.io/phppackages.org/symfony/flex/rank.svg)](http://phppackages.org/p/symfony/flex)
[![referenced-by](https://pages-proxy.gitlab.io/phppackages.org/symfony/flex/referenced-by.svg)](http://phppackages.org/p/symfony/flex)
Composer plugin for Symfony https://flex.symfony.com/

[[_TOC_]]

# About symfony/flex
* [*Fast, Smart Flex Recipe Upgrades with recipes:update*
  ](https://symfony.com/blog/fast-smart-flex-recipe-upgrades-with-recipes-update)
  2022-01 Ryan Weaver
* [*Upgrade Flex on your Symfony projects*
  ](https://symfony.com/blog/upgrade-flex-on-your-symfony-projects)
  2021-11 Fabien Potencier 
* [*Symfony Flex is Going Serverless*
  ](https://symfony.com/blog/symfony-flex-is-going-serverless)
  2021-09 Nicolas Grekas
* [*New in Symfony Flex 1.2*](https://symfony.com/blog/new-in-symfony-flex-1-2)
  2019-02 Fabien Potencier
* "auto-generated recipe" in Ch. 13 [*Installing Bundles with "Average" Docs*
  ](https://symfonycasts.com/screencast/symfony-fundamentals/slack-legacy-bundles)
  in [*Symfony 4 Fundamentals: Services, Config & Environments*
  ](https://symfonycasts.com/screencast/symfony-fundamentals)
  (2019) Ryan Weaver https://symfonycasts.com
* [*Symfony 4: Automate your Workflow*
  ](http://fabien.potencier.org/symfony4-workflow-automation.html)
  2017-04 Fabien Potencier

# Configuration
* `composer init`
* `composer config extra.symfony.allow-contrib true`
* `composer config extra.symfony.require "4.4.*"`

# Recipes by Rank
* <!--- phpunit/phpunit -->
  <!--- 1 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpunit/phpunit/badge/rank.svg)](http://phppackages.org/p/phpunit/phpunit)
  [**phpunit/phpunit**](https://github.com/symfony/recipes/tree/master/phpunit/phpunit)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpunit/phpunit/badge/referenced-by.svg)](http://phppackages.org/p/phpunit/phpunit)
* <!--- symfony/framework-bundle -->
  <!--- 24 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/framework-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/framework-bundle)
  [**symfony/framework-bundle**](https://github.com/symfony/recipes/tree/master/symfony/framework-bundle)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/framework-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/framework-bundle)
  * <!--- symfony/http-foundation -->
    <!--- 45 2019-11 -->
    [![PHPPackages Rank](http://phppackages.org/p/symfony/http-foundation/badge/rank.svg)](http://phppackages.org/p/symfony/http-foundation)
    **symfony/http-foundation**
    [![PHPPackages Referenced By](http://phppackages.org/p/symfony/http-foundation/badge/referenced-by.svg)](http://phppackages.org/p/symfony/http-foundation)
    http-foundation
* 91 doctrine/doctrine-bundle
  * doctrine/doctrine-cache-bundle (426, deprecated)
* 128 sensio/framework-extra-bundle
* 135 friendsofsymfony/user-bundle [@Symfony](https://symfony.com/doc/current/bundles/FOSUserBundle)
  * symfony/twig-bundle (227)
  * symfony/security-bundle (329)
* 166 symfony/monolog-bundle
* 227 ~~symfony/twig-bundle~~ -> 135 friendsofsymfony/user-bundle
* 232 friendsofsymfony/rest-bundle [@Symfony](https://symfony.com/doc/current/bundles/FOSRestBundle)
* 236 symfony/swiftmailer-bundle
* 262 sonata-project/admin-bundle [@Symfony](https://symfony.com/doc/current/bundles/SonataAdminBundle)
* 278 jms/serializer-bundle
* 298 sensio/generator-bundle
* 312 sensio/distribution-bundle
* 326 doctrine/doctrine-fixtures-bundle
* 329 ~~symfony/security-bundle~~ -> 135 friendsofsymfony/user-bundle
* 333 stof/doctrine-extensions-bundle [@Symfony](https://symfony.com/doc/current/bundles/StofDoctrineExtensionsBundle)
* 338 nelmio/api-doc-bundle [@Symfony](https://symfony.com/doc/current/bundles/NelmioApiDocBundle)
* <!--- hwi/oauth-bundle -->
  <!--- 408 2020-01 -->
  [![PHPPackages Rank](http://phppackages.org/p/hwi/oauth-bundle/badge/rank.svg)](http://phppackages.org/p/hwi/oauth-bundle)
  hwi/oauth-bundle
  [@Symfony](https://symfony.com/doc/current/security.html)
  https://gitlab.com/php-packages-demo/hwi-oauth-bundle
* 355 symfony/assetic-bundle
* 377 javiereguiluz/easyadmin-bundle
* 385 doctrine/doctrine-migrations-bundle
* 414 knplabs/knp-paginator-bundle
* 426 ~~doctrine/doctrine-cache-bundle~~ -> 91 doctrine/doctrine-bundle
* 430 knplabs/knp-menu-bundle [@Symfony](https://symfony.com/doc/current/bundles/KnpMenuBundle)
* 431 friendsofsymfony/jsrouting-bundle [@Symfony](https://symfony.com/doc/current/bundles/FOSJsRoutingBundle)
* 462 liip/imagine-bundle [@Symfony](https://symfony.com/doc/current/bundles/LiipImagineBundle)
* 470 vich/uploader-bundle
* 479 easycorp/easyadmin-bundle [@Symfony](https://symfony.com/doc/current/bundles/EasyAdminBundle)
* <!--- symfony/maker-bundle -->
  <!--- 537 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/symfony/maker-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/maker-bundle)
  **symfony/maker-bundle**
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/maker-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/maker-bundle)
* 548 symfony/web-profiler-bundle
* 557 snc/redis-bundle
* 562 friendsofsymfony/elastica-bundle
* 584 mopa/bootstrap-bundle
* 637 knplabs/knp-snappy-bundle
* 664 jms/di-extra-bundle
* 689 genemu/form-bundle
* 698 knplabs/knp-gaufrette-bundle
* 715 white-october/pagerfanta-bundle
* <!--- api-platform/core -->
  <!--- 762 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/api-platform/core/badge/rank.svg)](http://phppackages.org/p/api-platform/core)
  **api-platform/core**
  [![PHPPackages Referenced By](http://phppackages.org/p/api-platform/core/badge/referenced-by.svg)](http://phppackages.org/p/api-platform/core)
* 802 jms/security-extra-bundle
* 811 jms/translation-bundle
* 846 egeloen/ckeditor-bundle
  * 37664 friendsofsymfony/ckeditor-bundle [@Symfony](https://symfony.com/doc/current/bundles/FOSCKEditorBundle)
* 864 nelmio/security-bundle
* 985 jms/i18n-routing-bundle
* 992 avalanche123/imagine-bundle
* 994 liuggio/excelbundle
* 997 lexik/form-filter-bundle
* 1043 lexik/translation-bundle
* 1125 raulfraile/ladybug-bundle
* 1317 elao/web-profiler-extra-bundle
* 1477 exercise/htmlpurifier-bundle
* 1601 jiabin/julius-framework-extra-bundle
* 1615 symfony/web-server-bundle
* 1737 simplethings/form-extra-bundle
* 1739 symfony-cmf/routing-bundle
* <!--- api-platform/api-pack -->
  <!--- 14098 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/api-platform/api-pack/badge/rank.svg)](http://phppackages.org/p/api-platform/api-pack)
  **api-platform/api-pack**
  [![PHPPackages Referenced By](http://phppackages.org/p/api-platform/api-pack/badge/referenced-by.svg)](http://phppackages.org/p/api-platform/api-pack)
  api api-platform

# Recipes by "Referenced By"
* [![PHPPackages Referenced By](http://phppackages.org/p/phpunit/phpunit/badge/referenced-by.svg)](http://phppackages.org/p/phpunit/phpunit)
  **phpunit/phpunit**
  [![PHPPackages Rank](http://phppackages.org/p/phpunit/phpunit/badge/rank.svg)](http://phppackages.org/p/phpunit/phpunit)
* 2072 kitpages/data-grid-bundle
* 2592 symfony-cmf/routing-extra-bundle
* 2657 doctrine/doctrine-bundle
* [![PHPPackages Referenced By](http://phppackages.org/p/symfony/http-foundation/badge/referenced-by.svg)](http://phppackages.org/p/symfony/http-foundation)
  **symfony/http-foundation**
  [![PHPPackages Rank](http://phppackages.org/p/symfony/http-foundation/badge/rank.svg)](http://phppackages.org/p/symfony/http-foundation)
  http-foundation
* [![PHPPackages Referenced By](http://phppackages.org/p/api-platform/core/badge/referenced-by.svg)](http://phppackages.org/p/api-platform/core)
  **api-platform/core**
  [![PHPPackages Rank](http://phppackages.org/p/api-platform/core/badge/rank.svg)](http://phppackages.org/p/api-platform/core)
* [![PHPPackages Referenced By](http://phppackages.org/p/api-platform/api-pack/badge/referenced-by.svg)](http://phppackages.org/p/api-platform/api-pack)
  **api-platform/api-pack**
  [![PHPPackages Rank](http://phppackages.org/p/api-platform/api-pack/badge/rank.svg)](http://phppackages.org/p/api-platform/api-pack)
  api api-platform

# Sources for references
* [*New in Symfony Flex 1.2*](https://symfony.com/blog/new-in-symfony-flex-1-2)
* [*Symfony Bundles Documentation*](https://symfony.com/doc/bundles/)
* [symfony*-bundle](https://phppackages.org/s/symfony*-bundle)
* [*-bundle](https://phppackages.org/s/*-bundle)
* [*The 30 Most Useful Symfony Bundles (and making them even better)*](https://symfony.com/blog/the-30-most-useful-symfony-bundles-and-making-them-even-better)
  2014 Javier Eguiluz
